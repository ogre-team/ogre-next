#-------------------------------------------------------------------
# This file is part of the CMake build system for OGRE
#     (Object-oriented Graphics Rendering Engine)
# For the latest info, see http://www.ogre3d.org/
#
# The contents of this file are placed in the public domain. Feel
# free to make use of it in any way you like.
#-------------------------------------------------------------------

# Note about Debian packaging of this file:
# The name for variables has change from OGRE to OGRE-Next. For more information
# please read:
# https://salsa.debian.org/ogre-team/ogre-next/-/raw/master/debian/README.Debian

# - Try to find OGRE
# If you have multiple versions of Ogre installed, use the CMake or
# the environment variable OGRE-Next_HOME to point to the path where the
# desired Ogre version can be found.
# By default this script will look for a dynamic Ogre build. If you
# need to link against static Ogre libraries, set the CMake variable
# OGRE-Next_STATIC to TRUE.
#
# Once done, this will define
#
#  OGRE-Next_FOUND - system has OGRE
#  OGRE-Next_INCLUDE_DIRS - the OGRE include directories 
#  OGRE-Next_LIBRARIES - link these to use the OGRE core
#  OGRE-Next_BINARY_REL - location of the main Ogre binary (win32 non-static only, release)
#  OGRE-Next_BINARY_DBG - location of the main Ogre binaries (win32 non-static only, debug)
#
# Additionally this script searches for the following optional
# parts of the Ogre package:
#  Plugin_CgProgramManager, Plugin_ParticleFX, 
#  RenderSystem_GL, RenderSystem_GL3Plus,
#  RenderSystem_GLES, RenderSystem_GLES2,
#  RenderSystem_Direct3D9, RenderSystem_Direct3D11
#  Paging, Terrain, Volume, Overlay
#
# For each of these components, the following variables are defined:
#
#  OGRE-Next_${COMPONENT}_FOUND - ${COMPONENT} is available
#  OGRE-Next_${COMPONENT}_INCLUDE_DIRS - additional include directories for ${COMPONENT}
#  OGRE-Next_${COMPONENT}_LIBRARIES - link these to use ${COMPONENT} 
#  OGRE-Next_${COMPONENT}_BINARY_REL - location of the component binary (win32 non-static only, release)
#  OGRE-Next_${COMPONENT}_BINARY_DBG - location of the component binary (win32 non-static only, debug)
#
# Finally, the following variables are defined:
#
#  OGRE-Next_PLUGIN_DIR_REL - The directory where the release versions of
#       the OGRE plugins are located
#  OGRE-Next_PLUGIN_DIR_DBG - The directory where the debug versions of
#       the OGRE plugins are located
#  OGRE-Next_MEDIA_DIR - The directory where the OGRE sample media is
#       located, if available

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}")
include(FindPkgMacros)
include(PreprocessorUtils)
findpkg_begin(OGRE_Next)


# Get path, convert backslashes as ${ENV_${var}}
getenv_path(OGRE-Next_HOME)
getenv_path(OGRE-Next_SDK)
getenv_path(OGRE-Next_SOURCE)
getenv_path(OGRE-Next_BUILD)
getenv_path(OGRE-Next_DEPENDENCIES_DIR)
getenv_path(PROGRAMFILES)

# Determine whether to search for a dynamic or static build
if (OGRE-Next_STATIC)
  set(OGRE-Next_LIB_SUFFIX "Static")
else ()
  set(OGRE-Next_LIB_SUFFIX "")
endif ()

if(APPLE AND NOT OGRE-Next_STATIC)
	set(OGRE-Next_LIBRARY_NAMES "OgreNext${OGRE-Next_LIB_SUFFIX}")
else()
	set(OGRE-Next_LIBRARY_NAMES "OgreNextMain${OGRE-Next_LIB_SUFFIX}")
endif()
get_debug_names(OGRE-Next_LIBRARY_NAMES)
          
# construct search paths from environmental hints and
# OS specific guesses
if (WIN32)
  set(OGRE-Next_PREFIX_GUESSES
    ${ENV_PROGRAMFILES}/OGRE
    C:/OgreSDK
  )
elseif (UNIX)
  set(OGRE-Next_PREFIX_GUESSES
    /opt/ogre
    /opt/OGRE
    /usr/lib${LIB_SUFFIX}/ogre
    /usr/lib${LIB_SUFFIX}/OGRE
    /usr/local/lib${LIB_SUFFIX}/ogre
    /usr/local/lib${LIB_SUFFIX}/OGRE
    $ENV{HOME}/ogre
    $ENV{HOME}/OGRE
  )
  if (APPLE)
    set(OGRE-Next_PREFIX_GUESSES 
      ${CMAKE_CURRENT_SOURCE_DIR}/lib/${CMAKE_BUILD_TYPE}
      ${OGRE-Next_PREFIX_GUESSES}
    )
  endif ()
endif ()
set(OGRE-Next_PREFIX_PATH
  ${OGRE-Next_HOME} ${OGRE-Next_SDK} ${ENV_OGRE-Next_HOME} ${ENV_OGRE-Next_SDK}
  ${OGRE-Next_PREFIX_GUESSES}
)
create_search_paths(OGRE)
# If both OGRE-Next_BUILD and OGRE-Next_SOURCE are set, prepare to find Ogre in a build dir
set(OGRE-Next_PREFIX_SOURCE ${OGRE-Next_SOURCE} ${ENV_OGRE-Next_SOURCE})
set(OGRE-Next_PREFIX_BUILD ${OGRE-Next_BUILD} ${ENV_OGRE-Next_BUILD})
set(OGRE-Next_PREFIX_DEPENDENCIES_DIR ${OGRE-Next_DEPENDENCIES_DIR} ${ENV_OGRE-Next_DEPENDENCIES_DIR})
if (OGRE-Next_PREFIX_SOURCE AND OGRE-Next_PREFIX_BUILD)
  foreach(dir ${OGRE-Next_PREFIX_SOURCE})
    set(OGRE-Next_INC_SEARCH_PATH ${dir}/OgreMain/include ${dir}/Dependencies/include ${dir}/iOSDependencies/include ${dir}/AndroidDependencies/include ${OGRE-Next_INC_SEARCH_PATH})
    set(OGRE-Next_LIB_SEARCH_PATH ${dir}/lib ${dir}/Dependencies/lib ${dir}/iOSDependencies/lib ${dir}/AndroidDependencies/lib/${ANDROID_ABI} ${OGRE-Next_LIB_SEARCH_PATH})
    set(OGRE-Next_BIN_SEARCH_PATH ${dir}/Samples/Common/bin ${OGRE-Next_BIN_SEARCH_PATH})
  endforeach(dir)
  foreach(dir ${OGRE-Next_PREFIX_BUILD})
    set(OGRE-Next_INC_SEARCH_PATH ${dir}/include ${OGRE-Next_INC_SEARCH_PATH})
    if(APPLE AND NOT OGRE-Next_BUILD_PLATFORM_APPLE_IOS)
        set(OGRE-Next_LIB_SEARCH_PATH ${dir}/lib/macosx ${OGRE-Next_LIB_SEARCH_PATH})
    else()
        set(OGRE-Next_LIB_SEARCH_PATH ${dir}/lib ${OGRE-Next_LIB_SEARCH_PATH})
    endif()

    if (OGRE-Next_BUILD_PLATFORM_APPLE_IOS)
        set(OGRE-Next_LIB_SEARCH_PATH ${dir}/lib/iphoneos ${dir}/lib/iphonesimulator ${OGRE-Next_LIB_SEARCH_PATH})
    endif()

    set(OGRE-Next_BIN_SEARCH_PATH ${dir}/bin ${OGRE-Next_BIN_SEARCH_PATH})
    set(OGRE-Next_BIN_SEARCH_PATH ${dir}/Samples/Common/bin ${OGRE-Next_BIN_SEARCH_PATH})

    if(APPLE AND NOT OGRE-Next_BUILD_PLATFORM_APPLE_IOS)
      set(OGRE-Next_BIN_SEARCH_PATH ${dir}/bin/macosx ${OGRE-Next_BIN_SEARCH_PATH})
    endif()
  endforeach(dir)
  
  if (OGRE-Next_PREFIX_DEPENDENCIES_DIR)
    set(OGRE-Next_INC_SEARCH_PATH ${OGRE-Next_PREFIX_DEPENDENCIES_DIR}/include ${OGRE-Next_INC_SEARCH_PATH})
    set(OGRE-Next_LIB_SEARCH_PATH ${OGRE-Next_PREFIX_DEPENDENCIES_DIR}/lib ${OGRE-Next_LIB_SEARCH_PATH})
    set(OGRE-Next_BIN_SEARCH_PATH ${OGRE-Next_PREFIX_DEPENDENCIES_DIR}/bin ${OGRE-Next_BIN_SEARCH_PATH})
  endif()
else()
  set(OGRE-Next_PREFIX_SOURCE "NOTFOUND")
  set(OGRE-Next_PREFIX_BUILD "NOTFOUND")
endif ()

# redo search if any of the environmental hints changed
set(OGRE-Next_COMPONENTS Paging Terrain Volume Overlay 
  Plugin_CgProgramManager Plugin_ParticleFX
  RenderSystem_Direct3D11 RenderSystem_Direct3D9 RenderSystem_GL RenderSystem_GL3Plus RenderSystem_GLES RenderSystem_GLES2)
set(OGRE-Next_RESET_VARS 
  OGRE-Next_CONFIG_INCLUDE_DIR OGRE-Next_INCLUDE_DIR 
  OGRE-Next_FRAMEWORK_INCLUDES OGRE-Next_FRAMEWORK_PATH OGRE-Next_LIBRARY_FWK OGRE-Next_LIBRARY_REL OGRE-Next_LIBRARY_DBG
  OGRE-Next_PLUGIN_DIR_DBG OGRE-Next_PLUGIN_DIR_REL OGRE-Next_MEDIA_DIR)
foreach (comp ${OGRE-Next_COMPONENTS})
  set(OGRE-Next_RESET_VARS ${OGRE-Next_RESET_VARS}
    OGRE-Next_${comp}_INCLUDE_DIR OGRE-Next_${comp}_LIBRARY_FWK
    OGRE-Next_${comp}_LIBRARY_DBG OGRE-Next_${comp}_LIBRARY_REL
  )
endforeach (comp)
set(OGRE-Next_PREFIX_WATCH ${OGRE-Next_PREFIX_PATH} ${OGRE-Next_PREFIX_SOURCE} ${OGRE-Next_PREFIX_BUILD})
clear_if_changed(OGRE-Next_PREFIX_WATCH ${OGRE-Next_RESET_VARS})

if(NOT OGRE-Next_STATIC)
	# try to locate Ogre via pkg-config
	use_pkgconfig(OGRE-Next_PKGC "OGRE-Next${OGRE-Next_LIB_SUFFIX}")

	# Set the framework search path for OS X
    set(OGRE-Next_FRAMEWORK_SEARCH_PATH
      ${CMAKE_FRAMEWORK_PATH}
      ~/Library/Frameworks
      /Library/Frameworks
      /System/Library/Frameworks
      /Network/Library/Frameworks
      ${CMAKE_CURRENT_SOURCE_DIR}/lib/macosx/Release
      ${CMAKE_CURRENT_SOURCE_DIR}/lib/macosx/Debug
      ${CMAKE_CURRENT_SOURCE_DIR}/lib/${CMAKE_BUILD_TYPE}
    )
else()
	set(OGRE-Next_LIBRARY_FWK "")
endif()

# locate Ogre include files

find_path(OGRE-Next_CONFIG_INCLUDE_DIR NAMES OgreBuildSettings.h HINTS ${OGRE-Next_INC_SEARCH_PATH} ${OGRE-Next_FRAMEWORK_INCLUDES} ${OGRE-Next_PKGC_INCLUDE_DIRS} PATH_SUFFIXES "OGRE-Next")
message(STATUS "${OGRE-Next_CONFIG_INCLUDE_DIR}")
find_path(OGRE-Next_INCLUDE_DIR NAMES OgreRoot.h HINTS ${OGRE-Next_CONFIG_INCLUDE_DIR} ${OGRE-Next_INC_SEARCH_PATH} ${OGRE-Next_FRAMEWORK_INCLUDES} ${OGRE-Next_PKGC_INCLUDE_DIRS} PATH_SUFFIXES "OGRE-Next")
message(STATUS "${OGRE-Next_INCLUDE_DIR}")
set(OGRE-Next_INCOMPATIBLE FALSE)

if (OGRE-Next_INCLUDE_DIR)
  if (NOT OGRE-Next_CONFIG_INCLUDE_DIR)
    set(OGRE-Next_CONFIG_INCLUDE_DIR ${OGRE-Next_INCLUDE_DIR})
  endif ()
  # determine Ogre version
  file(READ ${OGRE-Next_INCLUDE_DIR}/OgrePrerequisites.h OGRE-Next_TEMP_VERSION_CONTENT)
  get_preprocessor_entry(OGRE-Next_TEMP_VERSION_CONTENT OGRE_VERSION_MAJOR OGRE-Next_VERSION_MAJOR)
  get_preprocessor_entry(OGRE-Next_TEMP_VERSION_CONTENT OGRE_VERSION_MINOR OGRE-Next_VERSION_MINOR)
  get_preprocessor_entry(OGRE-Next_TEMP_VERSION_CONTENT OGRE_VERSION_PATCH OGRE-Next_VERSION_PATCH)
  get_preprocessor_entry(OGRE-Next_TEMP_VERSION_CONTENT OGRE_VERSION_NAME OGRE_VERSION_NAME)
  set(OGRE-Next_VERSION "${OGRE-Next_VERSION_MAJOR}.${OGRE-Next_VERSION_MINOR}.${OGRE-Next_VERSION_PATCH}")
  pkg_message(OGRE "Found Ogre ${OGRE-Next_VERSION_NAME} (${OGRE-Next_VERSION})")

  # determine configuration settings
  set(OGRE-Next_CONFIG_HEADERS
    ${OGRE-Next_CONFIG_INCLUDE_DIR}/OgreBuildSettings.h
    ${OGRE-Next_CONFIG_INCLUDE_DIR}/OgreConfig.h
  )
  foreach(CFG_FILE ${OGRE-Next_CONFIG_HEADERS})
    if (EXISTS ${CFG_FILE})
      set(OGRE-Next_CONFIG_HEADER ${CFG_FILE})
      break()
    endif()
  endforeach()
  if (OGRE-Next_CONFIG_HEADER)
    file(READ ${OGRE-Next_CONFIG_HEADER} OGRE-Next_TEMP_CONFIG_CONTENT)
    has_preprocessor_entry(OGRE-Next_TEMP_CONFIG_CONTENT OGRE_STATIC_LIB OGRE-Next_CONFIG_STATIC)
    get_preprocessor_entry(OGRE-Next_TEMP_CONFIG_CONTENT OGRE_THREAD_SUPPORT OGRE-Next_CONFIG_THREADS)
    get_preprocessor_entry(OGRE-Next_TEMP_CONFIG_CONTENT OGRE_THREAD_PROVIDER OGRE-Next_CONFIG_THREAD_PROVIDER)
    get_preprocessor_entry(OGRE-Next_TEMP_CONFIG_CONTENT OGRE_NO_FREEIMAGE OGRE-Next_CONFIG_FREEIMAGE)
    if (OGRE-Next_CONFIG_STATIC AND OGRE-Next_STATIC)
    elseif (OGRE-Next_CONFIG_STATIC OR OGRE-Next_STATIC)
      pkg_message(OGRE "Build type (static, dynamic) does not match the requested one.")
      set(OGRE-Next_INCOMPATIBLE TRUE)
    endif ()
  else ()
    pkg_message(OGRE "Could not determine Ogre build configuration.")
    set(OGRE-Next_INCOMPATIBLE TRUE)
  endif ()
else ()
  set(OGRE-Next_INCOMPATIBLE FALSE)
endif ()

message(STATUS "1. waypoint ${OGRE-Next_INCOMPATIBLE}")

find_library(OGRE-Next_LIBRARY_REL NAMES ${OGRE-Next_LIBRARY_NAMES} HINTS ${OGRE-Next_LIB_SEARCH_PATH} ${OGRE-Next_PKGC_LIBRARY_DIRS} ${OGRE-Next_FRAMEWORK_SEARCH_PATH} PATH_SUFFIXES "" "Release" "RelWithDebInfo" "MinSizeRel")
find_library(OGRE-Next_LIBRARY_DBG NAMES ${OGRE-Next_LIBRARY_NAMES_DBG} HINTS ${OGRE-Next_LIB_SEARCH_PATH} ${OGRE-Next_PKGC_LIBRARY_DIRS} ${OGRE-Next_FRAMEWORK_SEARCH_PATH} PATH_SUFFIXES "" "Debug")

message(STATUS "REL ${OGRE-Next_LIBRARY_REL}")
message(STATUS "DBG ${OGRE-Next_LIBRARY_DBG}")
make_library_set(OGRE-Next_LIBRARY)

if (OGRE-Next_INCOMPATIBLE)
  set(OGRE-Next_LIBRARY "NOTFOUND")
endif ()

if("${OGRE-Next_FRAMEWORK_INCLUDES}" STREQUAL NOTFOUND)
  unset(OGRE-Next_FRAMEWORK_INCLUDES CACHE)
endif()
set(OGRE-Next_INCLUDE_DIR ${OGRE-Next_CONFIG_INCLUDE_DIR} ${OGRE-Next_INCLUDE_DIR} ${OGRE-Next_FRAMEWORK_INCLUDES})
list(REMOVE_DUPLICATES OGRE-Next_INCLUDE_DIR)
findpkg_finish(OGRE-Next)
add_parent_dir(OGRE-Next_INCLUDE_DIRS OGRE-Next_INCLUDE_DIR)
if (OGRE-Next_SOURCE)
	# If working from source rather than SDK, add samples include
	set(OGRE-Next_INCLUDE_DIRS ${OGRE-Next_INCLUDE_DIRS} "${OGRE-Next_SOURCE}/Samples/Common/include")
endif()

mark_as_advanced(OGRE-Next_CONFIG_INCLUDE_DIR OGRE-Next_MEDIA_DIR OGRE-Next_PLUGIN_DIR_REL OGRE-Next_PLUGIN_DIR_DBG)

message(STATUS "2. waypoint ${OGRE-Next_FOUND}")

if (NOT OGRE-Next_FOUND)
  return()
endif ()

message(STATUS "3. waypoint ${OGRE-Next_FOUND}")

# look for required Ogre dependencies in case of static build and/or threading
if (OGRE-Next_STATIC)
  set(OGRE-Next_DEPS_FOUND TRUE)
  find_package(Cg QUIET)
  find_package(DirectX QUIET)
  find_package(FreeImage QUIET)
  find_package(Freetype QUIET)
  find_package(OpenGL QUIET)
  find_package(OpenGLES QUIET)
  find_package(OpenGLES2 QUIET)
  find_package(ZLIB QUIET)
  find_package(ZZip QUIET)
  if (UNIX AND NOT APPLE AND NOT ANDROID)
    find_package(X11 QUIET)
    find_library(XAW_LIBRARY NAMES Xaw Xaw7 PATHS ${DEP_LIB_SEARCH_DIR} ${X11_LIB_SEARCH_PATH})
    if (NOT XAW_LIBRARY OR NOT X11_Xt_FOUND)
      set(X11_FOUND FALSE)
    endif ()
  endif ()

  set(OGRE-Next_LIBRARIES ${OGRE-Next_LIBRARIES} ${ZZip_LIBRARIES} ${ZLIB_LIBRARIES} ${FreeImage_LIBRARIES} ${FREETYPE_LIBRARIES})

  if (APPLE AND NOT OGRE-Next_BUILD_PLATFORM_APPLE_IOS AND NOT ANDROID)
    set(OGRE-Next_LIBRARIES ${OGRE-Next_LIBRARIES} ${X11_LIBRARIES} ${X11_Xt_LIBRARIES} ${XAW_LIBRARY} ${X11_Xrandr_LIB} ${Carbon_LIBRARIES} ${Cocoa_LIBRARIES})
  endif()
  
  if (NOT ZLIB_FOUND OR NOT ZZip_FOUND)
    set(OGRE-Next_DEPS_FOUND FALSE)
  endif ()
  if (NOT FreeImage_FOUND AND NOT OGRE-Next_CONFIG_FREEIMAGE)
    set(OGRE-Next_DEPS_FOUND FALSE)
  endif ()
  if (NOT FREETYPE_FOUND)
    set(OGRE-Next_DEPS_FOUND FALSE)
  endif ()
  if (UNIX AND NOT APPLE AND NOT ANDROID)
	if (NOT X11_FOUND)
      set(OGRE-Next_DEPS_FOUND FALSE)
	endif ()
  endif ()
endif()
  if (OGRE-Next_CONFIG_THREADS)
    if (OGRE-Next_CONFIG_THREAD_PROVIDER EQUAL 1)
      if (OGRE-Next_STATIC)
    	set(Boost_USE_STATIC_LIBS TRUE)
    	if(OGRE-Next_BUILD_PLATFORM_APPLE_IOS)
          set(Boost_USE_MULTITHREADED OFF)
        endif()
      endif()
      
      set(OGRE-Next_BOOST_COMPONENTS thread date_time)
      find_package(Boost COMPONENTS ${OGRE-Next_BOOST_COMPONENTS} QUIET)
      if(Boost_FOUND AND Boost_VERSION GREATER 104900)
        if(Boost_VERSION GREATER 105300)
            set(OGRE-Next_BOOST_COMPONENTS thread date_time system atomic chrono)
        else()
            set(OGRE-Next_BOOST_COMPONENTS thread date_time system chrono)
        endif()
      endif()

      find_package(Boost COMPONENTS ${OGRE-Next_BOOST_COMPONENTS} QUIET)
      if (NOT Boost_THREAD_FOUND)
        set(OGRE-Next_DEPS_FOUND FALSE)
      else ()
        set(OGRE-Next_LIBRARIES ${OGRE-Next_LIBRARIES} ${Boost_LIBRARIES})
        set(OGRE-Next_INCLUDE_DIRS ${OGRE-Next_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})
      endif ()
    elseif (OGRE-Next_CONFIG_THREAD_PROVIDER EQUAL 2)
      find_package(POCO QUIET)
      if (NOT POCO_FOUND)
        set(OGRE-Next_DEPS_FOUND FALSE)
      else ()
        set(OGRE-Next_LIBRARIES ${OGRE-Next_LIBRARIES} ${POCO_LIBRARIES})
        set(OGRE-Next_INCLUDE_DIRS ${OGRE-Next_INCLUDE_DIRS} ${POCO_INCLUDE_DIRS})
      endif ()
    elseif (OGRE-Next_CONFIG_THREAD_PROVIDER EQUAL 3)
      find_package(TBB QUIET)
      if (NOT TBB_FOUND)
        set(OGRE-Next_DEPS_FOUND FALSE)
      else ()
        set(OGRE-Next_LIBRARIES ${OGRE-Next_LIBRARIES} ${TBB_LIBRARIES})
        set(OGRE-Next_INCLUDE_DIRS ${OGRE-Next_INCLUDE_DIRS} ${TBB_INCLUDE_DIRS})
      endif ()
    endif ()
  endif ()
if (OGRE-Next_STATIC)
  if (NOT OGRE-Next_DEPS_FOUND)
    pkg_message(OGRE "Could not find all required dependencies for the Ogre package.")
    set(OGRE-Next_FOUND FALSE)
  endif ()
endif ()

if (NOT OGRE-Next_FOUND)
  return()
endif ()


get_filename_component(OGRE-Next_LIBRARY_DIR_REL "${OGRE-Next_LIBRARY_REL}" PATH)
get_filename_component(OGRE-Next_LIBRARY_DIR_DBG "${OGRE-Next_LIBRARY_DBG}" PATH)
set(OGRE-Next_LIBRARY_DIRS ${OGRE-Next_LIBRARY_DIR_REL} ${OGRE-Next_LIBRARY_DIR_DBG})

# find binaries
if (NOT OGRE-Next_STATIC)
	if (WIN32)
		find_file(OGRE-Next_BINARY_REL NAMES "OgreMain.dll" HINTS ${OGRE-Next_BIN_SEARCH_PATH}
          PATH_SUFFIXES "" Release RelWithDebInfo MinSizeRel)
		find_file(OGRE-Next_BINARY_DBG NAMES "OgreMain_d.dll" HINTS ${OGRE-Next_BIN_SEARCH_PATH}
          PATH_SUFFIXES "" Debug )
	endif()
	mark_as_advanced(OGRE-Next_BINARY_REL OGRE-Next_BINARY_DBG)
endif()


#########################################################
# Find Ogre components
#########################################################

set(OGRE-Next_COMPONENT_SEARCH_PATH_REL 
  ${OGRE-Next_LIBRARY_DIR_REL}/..
  ${OGRE-Next_LIBRARY_DIR_REL}/../..
  ${OGRE-Next_BIN_SEARCH_PATH}
)
set(OGRE-Next_COMPONENT_SEARCH_PATH_DBG
  ${OGRE-Next_LIBRARY_DIR_DBG}/..
  ${OGRE-Next_LIBRARY_DIR_DBG}/../..
  ${OGRE-Next_BIN_SEARCH_PATH}
)

macro(ogre_find_component COMPONENT HEADER PATH_HINTS)
  set(OGRE-Next_${COMPONENT}_FIND_QUIETLY ${OGRE-Next_FIND_QUIETLY})
  findpkg_begin(OGRE-Next_${COMPONENT})
  find_path(OGRE-Next_${COMPONENT}_INCLUDE_DIR NAMES ${HEADER} HINTS ${OGRE-Next_INCLUDE_DIRS} ${OGRE-Next_PREFIX_SOURCE} PATH_SUFFIXES ${PATH_HINTS} ${COMPONENT} OGRE/${COMPONENT} )
  set(OGRE-Next_${COMPONENT}_LIBRARY_NAMES "OgreNext${COMPONENT}${OGRE-Next_LIB_SUFFIX}")
  get_debug_names(OGRE-Next_${COMPONENT}_LIBRARY_NAMES)
  find_library(OGRE-Next_${COMPONENT}_LIBRARY_REL NAMES ${OGRE-Next_${COMPONENT}_LIBRARY_NAMES} HINTS ${OGRE-Next_LIBRARY_DIR_REL} ${OGRE-Next_FRAMEWORK_PATH} PATH_SUFFIXES "" "Release" "RelWithDebInfo" "MinSizeRel")
  find_library(OGRE-Next_${COMPONENT}_LIBRARY_DBG NAMES ${OGRE-Next_${COMPONENT}_LIBRARY_NAMES_DBG} HINTS ${OGRE-Next_LIBRARY_DIR_DBG} ${OGRE-Next_FRAMEWORK_PATH} PATH_SUFFIXES "" "Debug")
  make_library_set(OGRE-Next_${COMPONENT}_LIBRARY)
  findpkg_finish(OGRE-Next_${COMPONENT})
  if (OGRE-Next_${COMPONENT}_FOUND)
    # find binaries
    if (NOT OGRE-Next_STATIC)
	  if (WIN32)
	    find_file(OGRE-Next_${COMPONENT}_BINARY_REL NAMES "Ogre${COMPONENT}.dll" HINTS ${OGRE-Next_COMPONENT_SEARCH_PATH_REL} PATH_SUFFIXES "" bin bin/Release bin/RelWithDebInfo bin/MinSizeRel Release)
	    find_file(OGRE-Next_${COMPONENT}_BINARY_DBG NAMES "Ogre${COMPONENT}_d.dll" HINTS ${OGRE-Next_COMPONENT_SEARCH_PATH_DBG} PATH_SUFFIXES "" bin bin/Debug Debug)
	  endif()
	  mark_as_advanced(OGRE-Next_${COMPONENT}_BINARY_REL OGRE-Next_${COMPONENT}_BINARY_DBG)
    endif()
  endif()
  unset(OGRE-Next_${COMPONENT}_FIND_QUIETLY)
endmacro()

# look for Paging component
ogre_find_component(Paging OgrePaging.h "")
# look for Terrain component
ogre_find_component(Terrain OgreTerrain.h "")
# look for Property component
ogre_find_component(Property OgreProperty.h "")
# look for RTShaderSystem component
ogre_find_component(RTShaderSystem OgreRTShaderSystem.h "")
# look for Volume component
ogre_find_component(Volume OgreVolumePrerequisites.h "")
# look for Overlay component
ogre_find_component(Overlay OgreOverlaySystem.h "")
#look for HlmsPbs component
ogre_find_component(HlmsPbs OgreHlmsPbs.h Hlms/Pbs/)
#look for HlmsPbsMobile component
ogre_find_component(HlmsPbsMobile OgreHlmsPbsMobile.h Hlms/PbsMobile/)
#look for HlmsPbsMobile component
ogre_find_component(HlmsUnlit OgreHlmsUnlit.h Hlms/Unlit)
#look for HlmsUnlit component
ogre_find_component(HlmsUnlitMobile OgreHlmsUnlitMobile.h Hlms/UnlitMobile)

#########################################################
# Find Ogre plugins
#########################################################        
macro(ogre_find_plugin PLUGIN HEADER)
  # On Unix, the plugins might have no prefix
  if (CMAKE_FIND_LIBRARY_PREFIXES)
    set(TMP_CMAKE_LIB_PREFIX ${CMAKE_FIND_LIBRARY_PREFIXES})
    set(CMAKE_FIND_LIBRARY_PREFIXES ${CMAKE_FIND_LIBRARY_PREFIXES} "")
  endif()
  
  # strip RenderSystem_ or Plugin_ prefix from plugin name
  string(REPLACE "RenderSystem_" "" PLUGIN_TEMP ${PLUGIN})
  string(REPLACE "Plugin_" "" PLUGIN_NAME ${PLUGIN_TEMP})
  
  # header files for plugins are not usually needed, but find them anyway if they are present
  set(OGRE-Next_PLUGIN_PATH_SUFFIXES
    PlugIns PlugIns/${PLUGIN_NAME} Plugins Plugins/${PLUGIN_NAME} ${PLUGIN} 
    RenderSystems RenderSystems/${PLUGIN_NAME} ${ARGN})
  find_path(OGRE-Next_${PLUGIN}_INCLUDE_DIR NAMES ${HEADER} 
    HINTS ${OGRE-Next_INCLUDE_DIRS} ${OGRE-Next_PREFIX_SOURCE}  
    PATH_SUFFIXES ${OGRE-Next_PLUGIN_PATH_SUFFIXES})
  # find link libraries for plugins
  set(OGRE-Next_${PLUGIN}_LIBRARY_NAMES "${PLUGIN}${OGRE-Next_LIB_SUFFIX}")
  get_debug_names(OGRE-Next_${PLUGIN}_LIBRARY_NAMES)
  set(OGRE-Next_${PLUGIN}_LIBRARY_FWK ${OGRE-Next_LIBRARY_FWK})
  find_library(OGRE-Next_${PLUGIN}_LIBRARY_REL NAMES ${OGRE-Next_${PLUGIN}_LIBRARY_NAMES}
    HINTS "${OGRE-Next_BUILD}/lib" ${OGRE-Next_LIBRARY_DIRS} ${OGRE-Next_FRAMEWORK_PATH} PATH_SUFFIXES "" OGRE OGRE-${OGRE-Next_VERSION} opt Release Release/opt RelWithDebInfo RelWithDebInfo/opt MinSizeRel MinSizeRel/opt)
  find_library(OGRE-Next_${PLUGIN}_LIBRARY_DBG NAMES ${OGRE-Next_${PLUGIN}_LIBRARY_NAMES_DBG}
    HINTS "${OGRE-Next_BUILD}/lib" ${OGRE-Next_LIBRARY_DIRS} ${OGRE-Next_FRAMEWORK_PATH} PATH_SUFFIXES "" OGRE OGRE-${OGRE-Next_VERSION} opt Debug Debug/opt)
  make_library_set(OGRE-Next_${PLUGIN}_LIBRARY)

  if (OGRE-Next_${PLUGIN}_LIBRARY OR OGRE-Next_${PLUGIN}_INCLUDE_DIR)
    set(OGRE-Next_${PLUGIN}_FOUND TRUE)
    if (OGRE-Next_${PLUGIN}_INCLUDE_DIR)
      set(OGRE-Next_${PLUGIN}_INCLUDE_DIRS ${OGRE-Next_${PLUGIN}_INCLUDE_DIR})
    endif()
    set(OGRE-Next_${PLUGIN}_LIBRARIES ${OGRE-Next_${PLUGIN}_LIBRARY})
  endif ()

  mark_as_advanced(OGRE-Next_${PLUGIN}_INCLUDE_DIR OGRE-Next_${PLUGIN}_LIBRARY_REL OGRE-Next_${PLUGIN}_LIBRARY_DBG OGRE-Next_${PLUGIN}_LIBRARY_FWK)

  # look for plugin dirs
  if (OGRE-Next_${PLUGIN}_FOUND)
    if (NOT OGRE-Next_PLUGIN_DIR_REL OR NOT OGRE-Next_PLUGIN_DIR_DBG)
      if (WIN32)
        set(OGRE-Next_PLUGIN_SEARCH_PATH_REL 
          ${OGRE-Next_LIBRARY_DIR_REL}/..
          ${OGRE-Next_LIBRARY_DIR_REL}/../..
		  ${OGRE-Next_BIN_SEARCH_PATH}
        )
        set(OGRE-Next_PLUGIN_SEARCH_PATH_DBG
          ${OGRE-Next_LIBRARY_DIR_DBG}/..
          ${OGRE-Next_LIBRARY_DIR_DBG}/../..
		  ${OGRE-Next_BIN_SEARCH_PATH}
        )
        find_path(OGRE-Next_PLUGIN_DIR_REL NAMES "${PLUGIN}.dll" HINTS ${OGRE-Next_PLUGIN_SEARCH_PATH_REL}
          PATH_SUFFIXES "" bin bin/Release bin/RelWithDebInfo bin/MinSizeRel Release)
        find_path(OGRE-Next_PLUGIN_DIR_DBG NAMES "${PLUGIN}_d.dll" HINTS ${OGRE-Next_PLUGIN_SEARCH_PATH_DBG}
          PATH_SUFFIXES "" bin bin/Debug Debug)
      elseif (UNIX)
        get_filename_component(OGRE-Next_PLUGIN_DIR_TMP ${OGRE-Next_${PLUGIN}_LIBRARY_REL} PATH)
        set(OGRE-Next_PLUGIN_DIR_REL ${OGRE-Next_PLUGIN_DIR_TMP} CACHE STRING "Ogre plugin dir (release)" FORCE)
	    get_filename_component(OGRE-Next_PLUGIN_DIR_TMP ${OGRE-Next_${PLUGIN}_LIBRARY_DBG} PATH)
        set(OGRE-Next_PLUGIN_DIR_DBG ${OGRE-Next_PLUGIN_DIR_TMP} CACHE STRING "Ogre plugin dir (debug)" FORCE)  
      endif ()
    endif ()
	
	# find binaries
	if (NOT OGRE-Next_STATIC)
		if (WIN32)
			find_file(OGRE-Next_${PLUGIN}_REL NAMES "${PLUGIN}.dll" HINTS ${OGRE-Next_PLUGIN_DIR_REL})
			find_file(OGRE-Next_${PLUGIN}_DBG NAMES "${PLUGIN}_d.dll" HINTS ${OGRE-Next_PLUGIN_DIR_DBG})
		endif()
		mark_as_advanced(OGRE-Next_${PLUGIN}_REL OGRE-Next_${PLUGIN}_DBG)
	endif()
	
  endif ()

  if (TMP_CMAKE_LIB_PREFIX)
    set(CMAKE_FIND_LIBRARY_PREFIXES ${TMP_CMAKE_LIB_PREFIX})
  endif ()
endmacro(ogre_find_plugin)

ogre_find_plugin(Plugin_CgProgramManager OgreCgProgram.h PlugIns/CgProgramManager/include)
ogre_find_plugin(Plugin_ParticleFX OgreParticleFXPrerequisites.h PlugIns/ParticleFX/include)
ogre_find_plugin(RenderSystem_GL OgreGLRenderSystem.h RenderSystems/GL/include)
ogre_find_plugin(RenderSystem_GL3Plus OgreGL3PlusRenderSystem.h RenderSystems/GL3Plus/include)
ogre_find_plugin(RenderSystem_GLES OgreGLESRenderSystem.h RenderSystems/GLES/include)
ogre_find_plugin(RenderSystem_GLES2 OgreGLES2RenderSystem.h RenderSystems/GLES2/include)
ogre_find_plugin(RenderSystem_Direct3D9 OgreD3D9RenderSystem.h RenderSystems/Direct3D9/include)
ogre_find_plugin(RenderSystem_Direct3D11 OgreD3D11RenderSystem.h RenderSystems/Direct3D11/include)
        
if (OGRE-Next_STATIC)
  # check if dependencies for plugins are met
  if (NOT DirectX_FOUND)
    set(OGRE-Next_RenderSystem_Direct3D9_FOUND FALSE)
  endif ()
  if (NOT DirectX_D3D11_FOUND)
    set(OGRE-Next_RenderSystem_Direct3D11_FOUND FALSE)
  endif ()
  if (NOT OPENGL_FOUND)
    set(OGRE-Next_RenderSystem_GL_FOUND FALSE)
  endif ()
  if (NOT OPENGL_FOUND)
    set(OGRE-Next_RenderSystem_GL3Plus_FOUND FALSE)
  endif ()
  if (NOT OPENGLES_FOUND)
    set(OGRE-Next_RenderSystem_GLES_FOUND FALSE)
  endif ()
  if (NOT OPENGLES2_FOUND)
    set(OGRE-Next_RenderSystem_GLES2_FOUND FALSE)
  endif ()
  if (NOT Cg_FOUND)
    set(OGRE-Next_Plugin_CgProgramManager_FOUND FALSE)
  endif ()
  
  set(OGRE-Next_RenderSystem_Direct3D9_LIBRARIES ${OGRE-Next_RenderSystem_Direct3D9_LIBRARIES}
    ${DirectX_LIBRARIES}
  )

  set(OGRE-Next_RenderSystem_Direct3D11_LIBRARIES ${OGRE-Next_RenderSystem_Direct3D11_LIBRARIES}
    ${DirectX_D3D11_LIBRARIES}
  )
  set(OGRE-Next_RenderSystem_GL_LIBRARIES ${OGRE-Next_RenderSystem_GL_LIBRARIES}
    ${OPENGL_LIBRARIES}
  )
  set(OGRE-Next_RenderSystem_GL3Plus_LIBRARIES ${OGRE-Next_RenderSystem_GL3Plus_LIBRARIES}
    ${OPENGL_LIBRARIES}
  )
  set(OGRE-Next_RenderSystem_GLES_LIBRARIES ${OGRE-Next_RenderSystem_GLES_LIBRARIES}
    ${OPENGLES_LIBRARIES}
  )
  set(OGRE-Next_RenderSystem_GLES2_LIBRARIES ${OGRE-Next_RenderSystem_GLES2_LIBRARIES}
    ${OPENGLES2_LIBRARIES}
  )
  set(OGRE-Next_Plugin_CgProgramManager_LIBRARIES ${OGRE-Next_Plugin_CgProgramManager_LIBRARIES}
    ${Cg_LIBRARIES}
  )
endif ()

# look for the media directory
set(OGRE-Next_MEDIA_SEARCH_PATH
  ${OGRE-Next_SOURCE}
  ${OGRE-Next_LIBRARY_DIR_REL}/..
  ${OGRE-Next_LIBRARY_DIR_DBG}/..
  ${OGRE-Next_LIBRARY_DIR_REL}/../..
  ${OGRE-Next_LIBRARY_DIR_DBG}/../..
  ${OGRE-Next_PREFIX_SOURCE}
)
set(OGRE-Next_MEDIA_SEARCH_SUFFIX
  Samples/Media
  Media
  media
  share/OGRE/media
  share/OGRE/Media
)

clear_if_changed(OGRE-Next_PREFIX_WATCH OGRE-Next_MEDIA_DIR)
find_path(OGRE-Next_MEDIA_DIR NAMES packs/cubemapsJS.zip HINTS ${OGRE-Next_MEDIA_SEARCH_PATH}
  PATHS ${OGRE-Next_PREFIX_PATH} PATH_SUFFIXES ${OGRE-Next_MEDIA_SEARCH_SUFFIX})

